package com.gmail.jd4656.randomtp;

import io.papermc.lib.PaperLib;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.ChunkSnapshot;
import org.bukkit.HeightMap;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.popcraft.chunkyborder.BorderData;

import java.util.HashMap;
import java.util.Map;
import java.util.SplittableRandom;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class CommandRandomTP implements CommandExecutor {

    Map<UUID, CoordinateCache> cache = new HashMap<>();
    Map<UUID, Long> teleportCooldowns = new HashMap<>();
    SplittableRandom rand = new SplittableRandom();

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }

        Player player = (Player) sender;

        if (!player.hasPermission("randomtp.use")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        World world = player.getWorld();
        if (args.length > 0) {
            world = Bukkit.getWorld(args[0]);
            if (world == null) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That world does not exist.");
                return true;
            }
        }

        if (!player.hasPermission("randomtp.use." + world.getName().toLowerCase())) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to random tp in this world.");
            return true;
        }

        if (Main.chunkyBorder == null || !Main.chunkyBorder.getBorders().containsKey(world.getName())) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A WorldBorder must be set for this command to function.");
            return true;
        }

        //Location randLoc = getRandomLocation(world);
        getRandomLocationAsync(world).thenAccept(randLoc -> {
            if (randLoc == null) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Unable to find a suitable location to teleport to.");
                return;
            }

            int seconds = Main.plugin.getConfig().getInt("delay");
            int cooldown = Main.plugin.getConfig().getInt("cooldown");
            int cost = Main.plugin.getConfig().getInt("cost");
            long ticks = seconds * 20;

            if (teleportCooldowns.containsKey(player.getUniqueId())) {
                long cd = teleportCooldowns.get(player.getUniqueId());
                long remainingTime = (System.currentTimeMillis() - cd);
                long remainingTimeSeconds = Math.round(remainingTime / 1000);
                if (remainingTimeSeconds < cooldown) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Time before next teleport: " + ChatColor.RED + (cooldown - remainingTimeSeconds) + ((cooldown - remainingTimeSeconds) == 1 ? " second." : " seconds."));
                    return;
                }
                teleportCooldowns.remove(player.getUniqueId());
            }

            if (seconds < 1 || sender.hasPermission("randomtp.override")) {
                Economy econ = Main.getEconomy();
                if (econ != null && cost > 0) {
                    double balance = econ.getBalance(player);
                    if (balance < cost) {
                        player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A random teleport costs " + ChatColor.RED + "$" + cost + ChatColor.DARK_RED
                                + ". You have " + ChatColor.RED + "$" + balance);
                        return;
                    }

                    econ.withdrawPlayer(player, cost);
                    player.sendMessage(ChatColor.GOLD + "You've been charged " + ChatColor.RED + "$" + cost + ChatColor.GOLD + " for using random tp.");
                }
                sender.sendMessage(ChatColor.GOLD + "Teleporting to x: " + randLoc.getBlockX() + " y: " + randLoc.getBlockY() + " z: " + randLoc.getBlockZ());

                randLoc.setDirection(player.getLocation().getDirection());
                randLoc.setPitch(player.getLocation().getPitch());
                randLoc.setYaw(player.getLocation().getYaw());

                PaperLib.teleportAsync(player, randLoc).thenAccept(result -> {
                    if (result) {
                        player.sendMessage(ChatColor.GOLD + "Teleported!");
                        if (cooldown > 0 && !player.hasPermission("randomtp.override")) {
                            teleportCooldowns.put(player.getUniqueId(), System.currentTimeMillis());
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Failed to teleport");
                    }
                });
            } else {
                int taskId = Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, () -> {
                    if (!Main.pendingTeleports.containsKey(player.getUniqueId().toString())) return;
                    Main.pendingTeleports.remove(player.getUniqueId().toString());

                    Economy econ = Main.getEconomy();
                    if (econ != null && cost > 0) {
                        double balance = econ.getBalance(player);
                        if (balance < cost) {
                            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A random teleport costs " + ChatColor.RED + "$" + cost + ChatColor.DARK_RED
                                    + ". You have " + ChatColor.RED + "$" + balance);
                            return;
                        }

                        econ.withdrawPlayer(player, cost);
                        player.sendMessage(ChatColor.GOLD + "You've been charged " + ChatColor.RED + "$" + cost + ChatColor.GOLD + " for using random tp.");
                    }

                    player.sendMessage(ChatColor.GOLD + "Teleporting to x: " + randLoc.getBlockX() + " y: " + randLoc.getBlockY() + " z: " + randLoc.getBlockZ());

                    randLoc.setDirection(player.getLocation().getDirection());
                    randLoc.setPitch(player.getLocation().getPitch());
                    randLoc.setYaw(player.getLocation().getYaw());

                    PaperLib.teleportAsync(player, randLoc).thenAccept(result -> {
                       if (result) {
                           player.sendMessage(ChatColor.GOLD + "Teleported!");
                           if (cooldown > 0 && !player.hasPermission("randomtp.override")) {
                               teleportCooldowns.put(player.getUniqueId(), System.currentTimeMillis());
                           }
                       } else {
                           player.sendMessage(ChatColor.RED + "Failed to teleport");
                       }
                    });
                }, ticks);

                Main.pendingTeleports.put(player.getUniqueId().toString(), taskId);
                sender.sendMessage(ChatColor.GOLD + "Teleporting in " + ChatColor.RED + seconds + (seconds == 1 ? " second" : " seconds") + ChatColor.GOLD + ", don't move!");
            }

        });


            return true;
    }

    private boolean isSafe(Material material) {
        return !material.equals(Material.LAVA) && !material.equals(Material.WATER) && !material.equals(Material.AIR) && material.isSolid();
    }

    private CompletableFuture<Location> getRandomLocationAsync(World world) {
        return getRandomLocationAsync(world, 0, new CompletableFuture<>());
    }

    private CompletableFuture<Location> getRandomLocationAsync(World world, final int attempts, CompletableFuture<Location> completableFuture) {
        if (attempts >= 10) {
            if (cache.containsKey(world.getUID())) {
                Location loc = cache.get(world.getUID()).getRandomLocation();
                Material feet = loc.getBlock().getType();
                Material head = loc.clone().add(0, 2, 0).getBlock().getType();
                Block floor = loc.clone().subtract(0, 1, 0).getBlock();

                if (feet.isSolid() || head.isSolid()) return null;
                if (floor.getType().equals(Material.GRASS) && !isSafe(floor.getLocation().clone().subtract(0, 1, 0).getBlock().getType())) return null;
                if (floor.getType().equals(Material.TALL_GRASS) && !isSafe(floor.getLocation().clone().subtract(0, 2, 0).getBlock().getType())) return null;

                completableFuture.complete(loc);
                return completableFuture;
            }
        }

        BorderData border = Main.chunkyBorder.getBorders().get(world.getName());
        Location center = new Location(world, border.getCenterX(), 64, border.getCenterZ());

        double centerX = center.getX();
        double centerZ = center.getZ();
        double radiusX = border.getRadiusX();
        double radiusZ = border.getRadiusZ();

        double angle = rand.nextDouble() * 360;
        double radians = Math.toRadians(angle);
        int x = (int) (Math.round(centerX + (rand.nextDouble() * radiusZ * Math.cos(radians))) + 0.5);
        int z = (int) (Math.round(centerZ + (rand.nextDouble() * radiusX * Math.sin(radians))) + 0.5);


        PaperLib.getChunkAtAsync(world, x / 16, z / 16).thenAccept(chunk -> {
            ChunkSnapshot snapshot = chunk.getChunkSnapshot(true, false, false);
            int chunkX = rand.nextInt(16);
            int chunkZ = rand.nextInt(16);
            int y = snapshot.getHighestBlockYAt(chunkX, chunkZ);

            Location loc = chunk.getBlock(chunkX, y, chunkZ).getLocation().add(0.5, 0, 0.5);
            Material floor = chunk.getBlock(chunkX, y - 1, chunkZ).getType();
            Material feet = chunk.getBlock(chunkX, y, chunkZ).getType();
            Material head = chunk.getBlock(chunkX, y + 2, chunkZ).getType();

            if (feet.isSolid() || head.isSolid()) {
                getRandomLocationAsync(world, attempts + 1, completableFuture);
                return;
            }
            if (floor.equals(Material.GRASS) && !isSafe(chunk.getBlock(chunkX, y - 2, chunkZ).getType())) {
                getRandomLocationAsync(world, attempts + 1, completableFuture);
                return;
            }
            if (floor.equals(Material.TALL_GRASS) && !isSafe(chunk.getBlock(chunkX, y - 3, chunkZ).getType())) {
                getRandomLocationAsync(world, attempts + 1, completableFuture);
                return;
            }
            if (!isSafe(floor)) {
                getRandomLocationAsync(world, attempts + 1, completableFuture);
                return;
            }

            completableFuture.complete(loc);
        });

        return completableFuture;
    }

    private Location getRandomLocation(World world) {
        BorderData border = Main.chunkyBorder.getBorders().get(world.getName());
        Location center = new Location(world, border.getCenterX(), 64, border.getCenterZ());
        Location randLoc;
        int count = 0;

        double centerX = center.getX();
        double centerZ = center.getZ();
        double radiusX = border.getRadiusX();
        double radiusZ = border.getRadiusZ();

        while (count < 10) {
            count++;
            double angle = rand.nextDouble() * 360;
            double radians = Math.toRadians(angle);
            double x = Math.round(centerX + (rand.nextDouble() * radiusZ * Math.cos(radians))) + 0.5;
            double z = Math.round(centerZ + (rand.nextDouble() * radiusX * Math.sin(radians))) + 0.5;
            double y = world.getHighestBlockYAt((int) x, (int) z, HeightMap.WORLD_SURFACE) + 1;
            randLoc = new Location(world, x, y, z);

            Block floor = randLoc.clone().subtract(0, 1, 0).getBlock();
            Material feet = randLoc.getBlock().getType();
            Material head = randLoc.clone().add(0, 2, 0).getBlock().getType();

            if (feet.isSolid() || head.isSolid()) continue;
            if (floor.getType().equals(Material.GRASS) && !isSafe(floor.getLocation().clone().subtract(0, 1, 0).getBlock().getType())) continue;
            if (floor.getType().equals(Material.TALL_GRASS) && !isSafe(floor.getLocation().clone().subtract(0, 2, 0).getBlock().getType())) continue;
            if (!isSafe(floor.getType())) continue;

            if (!cache.containsKey(world.getUID())) {
                CoordinateCache newCache = new CoordinateCache();
                newCache.add(randLoc);
                cache.put(world.getUID(), newCache);
            }

            return randLoc;
        }

        if (cache.containsKey(world.getUID())) {
            randLoc = cache.get(world.getUID()).getRandomLocation();
            Material feet = randLoc.getBlock().getType();
            Material head = randLoc.clone().add(0, 2, 0).getBlock().getType();
            Block floor = randLoc.clone().subtract(0, 1, 0).getBlock();

            if (feet.isSolid() || head.isSolid()) return null;
            if (floor.getType().equals(Material.GRASS) && !isSafe(floor.getLocation().clone().subtract(0, 1, 0).getBlock().getType())) return null;
            if (floor.getType().equals(Material.TALL_GRASS) && !isSafe(floor.getLocation().clone().subtract(0, 2, 0).getBlock().getType())) return null;

            return randLoc;
        }

        return null;
    }
}
