package com.gmail.jd4656.randomtp;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CoordinateCache {
    List<Location> coordinates;
    private Random random;

    CoordinateCache() {
        coordinates = new ArrayList<>();
        random = new Random();
    }

    public void add(Location loc) {
        if (coordinates.contains(loc)) return;
        coordinates.add(loc);
        if (coordinates.size() > 100) coordinates.remove(0);
    }

    public Location getRandomLocation() {
        if (coordinates.isEmpty()) return null;

        return coordinates.get(random.nextInt(coordinates.size()));
    }
}
