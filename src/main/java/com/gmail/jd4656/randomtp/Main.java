package com.gmail.jd4656.randomtp;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.popcraft.chunkyborder.ChunkyBorder;

import java.util.HashMap;
import java.util.Map;

public class Main extends JavaPlugin {
    static Main plugin;
    static Map<String, Integer> pendingTeleports = new HashMap<>();
    private static Economy econ = null;
    static ChunkyBorder chunkyBorder = null;

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        if (!setupEconomy()) {
            getLogger().severe("Economy integration disabled due to no Vault dependency found!");
        }

        chunkyBorder = ((ChunkyBorder) Main.plugin.getServer().getPluginManager().getPlugin("ChunkyBorder"));

        if (chunkyBorder == null) {
            getLogger().warning("Unabled to find ChunkyBorder - disabling plugin");
            return;
        }

        getServer().getPluginManager().registerEvents(new EventListeners(), this);
        this.getCommand("randomtp").setExecutor(new CommandRandomTP());

        getLogger().info("RandomTP loaded.");
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Economy getEconomy() {
        return econ;
    }
}
