package com.gmail.jd4656.randomtp;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventListeners implements Listener {
    @EventHandler
    public void PlayerMoveEvent(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Location from = event.getFrom();
        Location to = event.getTo();
        if (Main.pendingTeleports.containsKey(player.getUniqueId().toString()) && (from.getBlockX() != to.getBlockX() || from.getBlockY() != to.getBlockY() || from.getBlockZ() != to.getBlockZ())) {
            Bukkit.getServer().getScheduler().cancelTask(Main.pendingTeleports.get(player.getUniqueId().toString()));
            Main.pendingTeleports.remove(player.getUniqueId().toString());
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You moved! Your teleport has been cancelled.");
        }
    }

    @EventHandler
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (Main.pendingTeleports.containsKey(player.getUniqueId().toString())) {
            Bukkit.getServer().getScheduler().cancelTask(Main.pendingTeleports.get(player.getUniqueId().toString()));
            Main.pendingTeleports.remove(player.getUniqueId().toString());
        }
    }

    @EventHandler
    public void EntityDamageEvent(EntityDamageEvent event) {
        if (event.getEntityType() != EntityType.PLAYER) return;
        Player player = (Player) event.getEntity();

        if (Main.pendingTeleports.containsKey(player.getUniqueId().toString())) {
            Bukkit.getServer().getScheduler().cancelTask(Main.pendingTeleports.get(player.getUniqueId().toString()));
            Main.pendingTeleports.remove(player.getUniqueId().toString());
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Your teleport has been cancelled due to taking damage.");
        }
    }
}
